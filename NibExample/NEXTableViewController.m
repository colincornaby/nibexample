//
//  NEXViewController.m
//  NibExample
//
//  Created by Colin Cornaby on 6/1/14.
//  Copyright (c) 2014 Colin Cornaby. All rights reserved.
//

#import "NEXTableViewController.h"
#import "NEXTableViewCell.h"

@interface NEXTableViewController ()

@end

@implementation NEXTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[NEXTableViewCell class] forCellReuseIdentifier:@"TableCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LittleTableCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TableCell"];
    
    [self.tableView registerClass:[NEXTableViewCell class] forCellReuseIdentifier:@"BigTableCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BigTableCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BigTableCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return 1;
    if(section==1)
        return 3;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NEXTableViewCell *cell = nil;
    if(indexPath.section==0)
        cell = [tableView dequeueReusableCellWithIdentifier:@"BigTableCell" forIndexPath:indexPath];
    else
        cell = [tableView dequeueReusableCellWithIdentifier:@"TableCell" forIndexPath:indexPath];
    
    //setup the cell
    
    //going to hardcode some data here
    if(indexPath.section==0)
    {
        cell.titleLabel.text = @"Gold Stars";
        cell.unitLabel.text = @"Gold Stars/Walnut";
    } else {
        switch (indexPath.row) {
            case 0:
                cell.titleLabel.text = @"Cats";
                cell.unitLabel.text = @"Fiddles/Harmonica";
                break;
            case 1:
                cell.titleLabel.text = @"Burgers";
                cell.unitLabel.text = @"Movies/Chair";
                break;
            case 2:
                cell.titleLabel.text = @"Tumbleweeds";
                cell.unitLabel.text = @"Boats/Boats";
                break;
                
            default:
                break;
        }
    }
    
    //just throw a number into the cell
    cell.value = @(rand()/4000.0f);
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
        return 110.0f;
    return 55.0f;
}

@end
