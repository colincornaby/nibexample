//
//  NEXTableViewCell.m
//  NibExample
//
//  Created by Colin Cornaby on 6/1/14.
//  Copyright (c) 2014 Colin Cornaby. All rights reserved.
//

#import "NEXTableViewCell.h"

@interface NEXTableViewCell ()
{
    NSNumber * _value;
}

@property NSNumberFormatter * numberFormatter;

@end

@implementation NEXTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib
{
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    self.numberFormatter.generatesDecimalNumbers = YES;
    
    [self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.numberFormatter setMaximumFractionDigits:2];
    [self.numberFormatter setMinimumFractionDigits:2];
    
    [self updateValueText];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(NSNumber *)value
{
    return [_value copy];
}

-(void)setValue:(NSNumber *)value
{
    _value = [value copy];
    [self updateValueText];
}

-(void)updateValueText
{
    valueLabel.text = [self.numberFormatter stringFromNumber:self.value];
}

@end
