//
//  main.m
//  NibExample
//
//  Created by Colin Cornaby on 6/1/14.
//  Copyright (c) 2014 Colin Cornaby. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NEXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NEXAppDelegate class]));
    }
}
