//
//  NEXTableViewCell.h
//  NibExample
//
//  Created by Colin Cornaby on 6/1/14.
//  Copyright (c) 2014 Colin Cornaby. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NEXTableViewCell : UITableViewCell
{
    IBOutlet UILabel * valueLabel;
}

//the properties to the UILabels we want end developers to be able
//to modify
@property (strong) IBOutlet UILabel * titleLabel;
@property (strong) IBOutlet UILabel * unitLabel;

//the value to display
@property (copy) NSNumber * value;

@end
